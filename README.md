# Komputer Store

In this assignment I created a static webpage that displays different PCs and gives the user the opportunity to buy them. 
You can earn money by working or taking up a loan.

## Appendix A

Requirements for the Komputer Store

*Bank*

- The bank shows a bank balance. (Use this to buy a PC)
- Outstanding loan. (This is your current loan)
- Get-a-loan button. Use this to request a loan
  - You cannot get a loan more than double your bank balance.
  - You cannot get a second loan until after byuing a PC.
- You must pay off your loan before getting a second one.

*Work*

- Pay is your current payment.
- Bank button. Transfers your pay to your bank.
  - If there is a loan, 10% of your pay goes to pay down your loan.
  - The rest gets then transferred to your bank.
- Work button increases your pay by 100kr on each click.
- Pay loan button. This button takes all your pay into paying down your loan.

*Laptops*

Uses a provided API for Laptop Data.

- Laptop selection. Choose which laptop to show.
  - PC specs are shown under the features section.
- Info section. This section displays an image of the PC, a description and title, and a BUY NOW button.
- BUY NOW button. Clicking this button will make a purchase of the PC. The money will automatically be drawn from your bank balance.
  - If there is insufficient funds, the purchase will not go through.
  
