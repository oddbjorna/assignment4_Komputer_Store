// Variables to keep track of various data.
let balance = 100;
let pay = 0;
let currentLoan = 0;
let addedLoan;
let loan = false;
let listOfPcs;
let filteredList;
let computerBought = true;

// Base url for accessing the provided API
const baseURL = "https://noroff-komputer-store-api.herokuapp.com";

// Accessing different DOM elements.
const currentBalance = document.querySelector("#info-user-balance");
const loanbtn = document.querySelector("#loan-btn");
const overlay = document.querySelector(".overlay");
const cancelBtn = document.querySelector(".cancel-btn");
const form = document.querySelector(".inner-overlay");
const input = document.querySelector("#loan-amount");
const outstandingLoan = document.querySelector("#info-user-loan");
const loanDiv = [...document.getElementsByClassName("hidden")];
const bankButton = document.querySelector("#bank-btn");
const workButton = document.querySelector("#work-btn");
const workPay = document.querySelector("#work-pay");
const payLoanButton = document.querySelector("#pay-loan");
const laptopSelect = document.querySelector("#laptops-select");
const buyNowButton = document.querySelector("#info-laptop-button");
const laptopFeatures = document.querySelector("#laptops-features");
const laptopImage = document.querySelector("#laptop-image");
const laptopName = document.querySelector("#info-laptop-name");
const laptopDescription = document.querySelector("#info-laptop-description");
const laptopPrice = document.querySelector("#info-laptop-price");

/* Toggle the loan form and the outstanding loan field,
as well as the pay loa button to be hidden or shown. */
const toggleOverlay = () => {
  overlay.classList.toggle("visible");
};
const toggleLoan = () => {
  loanDiv.forEach((element) => {
    element.classList.toggle("hidden");
  });
};

// Updating the current balance for the user wheras value comes from pay balance.
const updateBalance = (value) => {
  balance = balance + value;
  currentBalance.innerText = balance + "kr";
};

/* Submit a loan request to the bank.
The bank will not supply a loan if 1 of following conditions occur:
- The user already has a loan
- The user has not bought a PC since he took up a loan.
- The requested loan is bigger than balance times 2 
- The user inputs NaN character*/
const submit = (event) => {
  event.preventDefault();
  if (loan) {
    return alert("Must pay your current loan before taking a new one!");
  }

  if (!computerBought) {
    return alert("You must buy a computer before taking another loan");
  }

  if (addedLoan === undefined || isNaN(addedLoan) || addedLoan < 1)
    return alert("Please enter a positive number");
  else if (addedLoan > balance * 2) {
    return alert(
      "Cannot do a loan bigger than the double of current bank balance"
    );
  }
  addedLoan = parseInt(addedLoan);
  updateBalance(addedLoan);

  loan = true;
  computerBought = false;
  currentLoan = addedLoan;
  outstandingLoan.innerText = currentLoan + "kr";

  toggleOverlay();
  toggleLoan();
};

// Updating the addedLoan variable when the input field changes.
const updateValue = (e) => {
  addedLoan = e.target.value;
};

/* Paying of the outstanding loan debt.
- If the current loan is less than or equal to pay balance,
subtract the loan amount from pay and subtract the loan amount from current loan.
- If not then set pay balance to 0 and subract the pay amount from the current loan.
- If the current loan is less than equal to 0 after subtracting, set loan to be 0,
and set the loan variable to be false.*/
const payLoan = () => {
  let tempPay = pay;

  if (currentLoan <= pay) {
    pay -= currentLoan;
    currentLoan -= tempPay;
  } else {
    pay = 0;
    currentLoan -= tempPay;
  }

  if (currentLoan <= 0) {
    currentLoan = 0;
    loan = false;
    toggleLoan();
  }

  workPay.innerText = pay + "kr";
  outstandingLoan.innerText = currentLoan + "kr";
};

/* Transfering to the bank balance.
- If current loan is less than 10% of pay balance,
set loan to be false, subtract the loan from pay balance and set current loan to be 0.
Update the bank balance with the remaining pay and set pay to be 0.
- If not the subtract 10% of pay from pay balance and current loan.
transfer the remainder to the bank balance.
- If the user does not have a loan, transfer full amount to bank.*/
const bankTransfer = () => {
  if (loan) {
    let percentage = pay * 0.1;

    if (percentage > currentLoan) {
      loan = false;
      pay -= currentLoan;
      currentLoan = 0;
      toggleLoan();

      updateBalance(pay);

      pay = 0;
      workPay.innerText = pay + "kr";
      outstandingLoan.innerText = currentLoan + "kr";
      console.log("hello");
    } else {
      currentLoan -= percentage;
      pay -= percentage;
      updateBalance(pay);
      pay = 0;
      workPay.innerText = pay + "kr";
      outstandingLoan.innerText = currentLoan + "kr";
    }
  } else {
    updateBalance(pay);
    pay = 0;
    workPay.innerText = pay + "kr";
  }
};

// Working to increase pay balance by 100kr.
const work = () => {
  pay += 100;
  workPay.innerText = pay + "kr";
};

/* Accessing the provided API for inserting data into the laptops section.
Gathers the entire set of PCs and saves them in a global variable.
So we only need to send 1 request to work with the data.
Also creates the option elements and puts them in the select element.
Runs genFeatures that is defined under.*/
const getPcs = async () => {
  try {
    const fetchPcs = await fetch(baseURL + "/computers");
    listOfPcs = await fetchPcs.json();
    listOfPcs.map((element) => {
      const optionElement = document.createElement("option");
      optionElement.innerText = element.title;
      laptopSelect.appendChild(optionElement);
    });
    genFeatures(laptopSelect.firstElementChild.innerText);
  } catch (error) {
    console.error(error);
  }
};

/* Genereating the features and info for the chosen PC.
Checking if the 'option' value is a string because when the page first loads,
a change event does not happen on the select item.*/
const genFeatures = (option) => {
  if (typeof option == "string") {
    filteredList = listOfPcs.filter((chosen) => chosen.title == option);
  } else {
    filteredList = listOfPcs.filter(
      (chosen) => chosen.title == option.target.value
    );
  }
  laptopFeatures.innerHTML = "";
  filteredList.map(async (element) => {
    // Adding features to the web page
    element.specs.forEach((feature) => {
      const listElement = document.createElement("li");
      listElement.innerText = feature;
      laptopFeatures.appendChild(listElement);
    });

    // Adding src to the img tag
    try {
      const image = await fetch(baseURL + "/" + element.image);
      if (!image.ok) {
        laptopImage.src = "https://placekitten.com/240/240";
      } else {
        laptopImage.src = baseURL + "/" + element.image;
      }
    } catch (error) {
      console.log(error);
    }

    // Adding a title
    laptopName.innerText = element.title;

    // Adding a description
    laptopDescription.innerText = element.description;

    // Adding a price
    laptopPrice.innerText = element.price + "kr";
  });
};

/* Buying a PC
Checks if the user has enough money in his bank account to buy the selected PC.
If it is, subtract the money from the users bank account and give an alert.
If not an alert will prompt.*/
const buy = () => {
  const price = filteredList[0].price;
  if (balance >= price) {
    balance -= price;
    currentBalance.innerText = balance + "kr";
    computerBought = true;
    alert("You bought: " + filteredList[0].title);
  } else {
    alert("you can't afford the: " + filteredList[0].title);
  }
};

/* Main function for readability and structure.
Updating balance on load, as well as fetches the PCs from the API.
Adds event listeners to all the required components.*/
const main = () => {
  updateBalance(0);
  getPcs();

  loanbtn.addEventListener("click", toggleOverlay);
  cancelBtn.addEventListener("click", toggleOverlay);
  input.addEventListener("change", updateValue);
  form.addEventListener("submit", submit);
  bankButton.addEventListener("click", bankTransfer);
  workButton.addEventListener("click", work);
  payLoanButton.addEventListener("click", payLoan);
  buyNowButton.addEventListener("click", buy);
  laptopSelect.addEventListener("change", genFeatures);
};

// Run main on load.
main();
